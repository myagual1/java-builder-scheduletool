CS542 Design Patterns
Spring 2016
PROJECT 3  README FILE

Due Date: Thursday, October 17, 2016
Submission Date: Thursday, October 17, 2016
Grace Period Used This Project: 0 Days
Grace Period Remaining: 0 Days
Author(s): Christopher Fu and Mariuxi Yagual
e-mail(s): myagual1@binghamton.edu and cfu6@binghamton.edu


PURPOSE:

[
    The purpose is to develop a tool for student orientation program at Binghamton University. It calculates the total time duration, cost, and effort of the orientation, based on her/his choices.
    No data structures were needed, data was stored in the concrete classes used to make up the itinerary. 
]

PERCENT COMPLETE:
[
    We believe we have completed 100% of this project
]

PARTS THAT ARE NOT COMPLETE:
[

]

BUGS:
[
    None                                
]
               
FILES:
[
    README, the text file you are presently reading          
]

SAMPLE OUTPUT:
[
 	run:
     [java] ITINERARY RESULTS:
     [java] ------------------
     [java] Total cost is 1003.2035 dollars.
     [java] Total duration is 230.0 minutes.
     [java] Total carbon footprint is 336.6
     [java] Total effort is 1084.4 calories.

BUILD SUCCESSFUL
Total time: 0 seconds
Mariuxis-MacBook-Air:studentOrientation mariuxiyagual$ 
 	   
]

TO COMPILE:
[
	Assuming you are in the directory containing this README:
	Just extract the files and then do "ant -buildfile src/build.xml all".
]

TO RUN:
[   
	Assuming you are in the directory containing this README:                                                                    
    To run by specifying arguments from command line [similarly for the 2nd argument and so on ...]
    We will use this to run your code
     ant -buildfile src/build.xml run -Darg0=firstarg 
     
    To run by specifying args in build.xml (just for debugging, not for submission)
	 ant -buildfile src/build.xml run
]

EXTRA CREDIT:
[                                                                                           
  N/A                                                                                             
]                                                                                               
                                                                                                
BIBLIOGRAPHY:
[
 	We only consulted the PowerPoints and advice from the professor                                                                                                                                
]

ACKNOWLEDGEMENT:
[
	Prof. Govindaraju
]                                   

Assuming you are in the directory containing this README:

## To clean:
ant -buildfile src/build.xml clean

## To compile: 
ant -buildfile src/build.xml all

## To run by specifying arguments from command line [similarly for the 2nd argument and so on ...]
## We will use this to run your code
ant -buildfile src/build.xml run -Darg0=firstarg 

## To run by specifying args in build.xml (just for debugging, not for submission)
ant -buildfile src/build.xml run

## To create tarball for submission
ant -buildfile src/build.xml tarzip


