package studentOrientation.classes;

/**
 *  Interface for cost
 *  required to impement 4 functions
 * */
public interface CostInterface {
	public float getTotal();
	public void setTotal(float num);
	public void addtoCost(float num);
	public String toString();
}
