package studentOrientation.classes;

/**
 *  Interface for effort
 *  Requires 4 methods to be implemented 
 * */
public interface EffortInterface {
	public float getTotal();
	public void setTotal(float num);
	public void addtoEffort(float num);
	public String toString();
}
