package studentOrientation.classes;
import studentOrientation.enums.dormOptions;

/**
 *  Interface for Dorm Activity
 *  Has one method, setup, which is called by the constructor
 * */
public interface DormActivityInterface{
	public void setup(dormOptions d, CostInterface co, CfInterface cf, DurationInterface du, EffortInterface ef);
}
