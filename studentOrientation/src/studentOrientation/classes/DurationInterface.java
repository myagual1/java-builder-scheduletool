package studentOrientation.classes;

public interface DurationInterface {

	public float getTotal();
	public void setTotal(float num);
	public void addtoDuration(float num);
	public String toString();
}
