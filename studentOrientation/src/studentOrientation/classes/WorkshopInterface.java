package studentOrientation.classes;
/**
 *  Interface for workshop
 *  Requires that one method, construct, is implemented in subclasses
 * */
public interface WorkshopInterface{
	public void construct(OOItinerary it);
}
