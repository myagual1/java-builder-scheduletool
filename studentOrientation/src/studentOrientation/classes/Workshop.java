package studentOrientation.classes;

/**
 *  Workshop concrete class
 *  Has one method construct
 * */
public class Workshop implements WorkshopInterface{
    /**
     *  Construct - Takes an itinerary and constructs it using a set order
     *  @param OOItinerary it
     * */
    public void construct(OOItinerary it){
        it.buildDuration();
        it.buildCarbonFootprint();
        it.buildCost();
        it.buildEffort();
            
        it.buildBookstoreActivity();
        it.buildTourActivity();
        it.buildRegistrationActivity();
        it.buildDormActivity();
    }
}
