package studentOrientation.classes;
import studentOrientation.enums.tourOptions;

public class TourActivity implements TourActivityInterface{
	
	private tourOptions t;
    private CostInterface co; 
    private CfInterface cf; 
    private DurationInterface du; 
    private EffortInterface ef;
    
    /**
     *  Constructor
     *  @Param regOptions rin
     *  @Param CostInterface coin
     *  @Param CfInterface cfin
     *  @Param DurationInterface duin
     *  @Param EffortInterface efin
     * */
    public 	TourActivity(tourOptions tin, CostInterface coin, CfInterface cfin, DurationInterface duin, EffortInterface efin){
  	    setup(tin,coin,cfin,duin,efin);
    }
    
    /**
     *  setup - assigns private variables and passes proper values to co, cf, duin, and ef
     *  @Param bookstoreName bin
     *  @Param CostInterface coin
     *  @Param CfInterface cfin
     *  @Param DurationInterface duin
     *  @Param EffortInterface efin
	*/
    public void setup(tourOptions tin, CostInterface coin, CfInterface cfin, DurationInterface duin, EffortInterface efin){
		 t=tin;
	     co=coin;
	     cf=cfin;
	     du=duin;
	     ef=efin;

		 float priceOftour; //Hardcoded value since was not specified in instructions
		 if(t == tourOptions.BUS){                                                                                            
	    	  priceOftour = 2.0f;  
	    	  co.addtoCost(priceOftour);
		      cf.addtoCF((float)200.0);
		      du.addtoDuration((float)40.0);
		      ef.addtoEffort((float)56.0);	
	       } 
	       else if(t == tourOptions.FOOT){
	    	   priceOftour = 0.1f; 
	    	   co.addtoCost(priceOftour);
		       cf.addtoCF((float)59.4);
		       du.addtoDuration((float)110.0);
		       ef.addtoEffort((float)570.7);	   
	       } 
	       else{
	           System.out.println("err");
	           System.exit(0);
	       } 
	}
}
