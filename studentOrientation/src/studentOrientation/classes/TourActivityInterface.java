package studentOrientation.classes;
import studentOrientation.enums.tourOptions;

/**
 *  Interface for tour activities
 *  Requires one method,  setup, to be implemented.
 * */
public interface TourActivityInterface{
	public void setup(tourOptions t, CostInterface co, CfInterface cf, DurationInterface du, EffortInterface ef);
}
