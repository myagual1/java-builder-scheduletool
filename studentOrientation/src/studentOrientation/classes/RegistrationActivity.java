package studentOrientation.classes;
import studentOrientation.enums.regOptions;

public class RegistrationActivity implements RegistrationActivityInterface{
	private regOptions r;
    private CostInterface co; 
    private CfInterface cf; 
    private DurationInterface du; 
    private EffortInterface ef;
    
    /**
     *  Constructor
     *  @Param regOptions rin
     *  @Param CostInterface coin
     *  @Param CfInterface cfin
     *  @Param DurationInterface duin
     *  @Param EffortInterface efin
     * */
    public RegistrationActivity(regOptions rin, CostInterface coin, CfInterface cfin, DurationInterface duin, EffortInterface efin){
  	    setup(rin,coin,cfin,duin,efin);
    }
     
    /**
     *  setup - assigns private variables and passes proper values to co, cf, duin, and ef
     *  @Param bookstoreName bin
     *  @Param CostInterface coin
     *  @Param CfInterface cfin
     *  @Param DurationInterface duin
     *  @Param EffortInterface efin
     * */
	public void setup(regOptions rin, CostInterface coin, CfInterface cfin, DurationInterface duin, EffortInterface efin){
		  r=rin;
	      co=coin;
	      cf=cfin;
	      du=duin;
	      ef=efin;

		  float priceOfreg = (float)250.0;//Hardcoded value since was not specified in instructions
	      if(r == regOptions.FORM){                                                                                            
	    	  priceOfreg -= priceOfreg * (float)0.03;  
	    	  co.addtoCost(priceOfreg);
		      cf.addtoCF((float)67.0);
		      du.addtoDuration((float)30.0);
		      ef.addtoEffort((float)234.6);	
	       } 
	       else if(r == regOptions.LAB){
	    	   co.addtoCost(priceOfreg);
		       cf.addtoCF((float)89);
		       du.addtoDuration((float)100);
		       ef.addtoEffort((float)250);	   
	       } 
	       else{
	           System.out.println("err");
	           System.exit(0);
	       } 
	}	
}
