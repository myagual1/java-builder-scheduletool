package studentOrientation.classes;
import studentOrientation.enums.bookstoreName;
import studentOrientation.enums.dormOptions;
import studentOrientation.enums.regOptions;
import studentOrientation.enums.tourOptions;

/**
 *  Abstract class for itinerary
 *  Has private enums and abstract methods to be implemented
 * */
public abstract class OOItinerary{
	private bookstoreName b1;
	private tourOptions t1;
	private dormOptions d1;
	private regOptions r1;
	
	public abstract void setDescription(bookstoreName b1, tourOptions t1, dormOptions d1, regOptions r1);
	abstract public void buildDuration();
	abstract public void buildCarbonFootprint();
	abstract public void buildCost();
	abstract public void buildEffort();
        
	abstract public void buildBookstoreActivity();
	abstract public void buildTourActivity();
	abstract public void buildRegistrationActivity();
	abstract public void buildDormActivity();
	abstract public void returnResults();
}
