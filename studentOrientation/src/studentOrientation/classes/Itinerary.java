package studentOrientation.classes;
import studentOrientation.enums.bookstoreName;
import studentOrientation.enums.dormOptions;
import studentOrientation.enums.regOptions;
import studentOrientation.enums.tourOptions;

public class Itinerary extends OOItinerary{
	bookstoreName b1;
	tourOptions t1;
	dormOptions d1;
	regOptions r1;

    private DurationInterface d;
    private CfInterface cf;
    private CostInterface c;
    private EffortInterface e;

    /**
     * Constructor
     * @param bookstoreName enum for book store name
     * @param tourOptions enum for tour option
     * @param dormOptions enum for dorm option
     * @param regOptions enum for registration option
     * */
	public Itinerary(bookstoreName b1, tourOptions t1, dormOptions d1, regOptions r1){
        setDescription(b1,t1,d1,r1);
    }

    /**
     * setDescription 
     * Called on by the constructor to set private variables.
     * @param bookstoreName enum for book store name
     * @param tourOptions enum for tour option
     * @param dormOptions enum for dorm option
     * @param regOptions enum for registration option
     * */
    public void setDescription(bookstoreName b1in, tourOptions t1in, dormOptions d1in, regOptions r1in){
        b1=b1in;
        t1=t1in;
        d1=d1in;
        r1=r1in;
    }
    
    /**
     *  Constructs new duration concrete class and saves it to private variable d
     * */
	public void buildDuration(){
        d = new Duration();
    }

    /**
     *  Constructs new cf concrete class and saves it to private variable cf
     * */
	public void buildCarbonFootprint(){
        cf = new CarbonFootprint();
    }
    
    /**
     *  Constructs new cost concrete class and saves it to private variable c
     * */
	public void buildCost(){
        c = new Cost();
    }
    
    /**
     *  Constructs new effort concrete class and saves it to private variable e
     * */
	public void buildEffort(){
        e = new Effort();
    }
        
    /**
     *  Constructs new Bookstore Activity concrete class
     * */
	public void buildBookstoreActivity(){
        BookstoreActivityInterface bsi = new BookstoreActivity(b1,c,cf,d,e);
	}

    /**
     *  Constructs new Tour Activity concrete class
     * */
	public void buildTourActivity(){
        DormActivityInterface dsi = new DormActivity(d1, c, cf, d, e);
    }

    /**
     *  Constructs new Registration Activity concrete class
     * */
	public void buildRegistrationActivity(){
        RegistrationActivityInterface rsi = new RegistrationActivity(r1, c, cf, d, e);
    }
    
    /**
     *  Constructs new Dorm Activity concrete class
     * */
	public void buildDormActivity(){
        TourActivityInterface tsi = new TourActivity(t1, c, cf, d, e);
    }

    /**
     * returnResults
     * This function prints the total cost, cf, effort, and duration of the schedule. 
     * Takes no params and no returns
     * */
    public void returnResults(){
        System.out.println("ITINERARY RESULTS:");
        System.out.println("------------------");
        System.out.println(c.toString());
        System.out.println(d.toString());
        System.out.println(cf.toString());
        System.out.println(e.toString());
    }

    /**
     *  toString
     * */
    public String toString(){
        return c.toString() + d.toString() + cf.toString() + e.toString();
    }

    
}
