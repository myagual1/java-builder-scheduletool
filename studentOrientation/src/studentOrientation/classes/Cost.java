package studentOrientation.classes;

public class Cost implements CostInterface{
	private float total;
	
    /**
     * Constructor - sets initial cost
     * */
    public Cost(){
		total = 0;
	}

    /**
     *  Constructor - sets inital cost to given cost
     *  @param float c
     * */
	public Cost(float c){
		total = c;
	}

    /**
     *  getTotal
     *  @return total
     * */
	public float getTotal(){
		return total;
	}

    /**
     *  setTotal
     *  @param float num
     * */
	public void setTotal(float num){
		total = num;
	}

    /**
     *  setTotal
     *  @param float num
     * */
	public void addtoCost(float num){
		total += num;
	}
    
    /**
     *  toString
     * */
	public String toString(){
		return "Total cost is " + getTotal() + " dollars.";
	}
}
