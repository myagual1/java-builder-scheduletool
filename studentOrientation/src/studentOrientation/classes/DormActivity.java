package studentOrientation.classes;
import studentOrientation.enums.dormOptions;

public class DormActivity implements DormActivityInterface{
	private dormOptions d;
    private CostInterface co; 
    private CfInterface cf; 
    private DurationInterface du; 
    private EffortInterface ef;
    
    /**
     *  Constructor
     *  @Param dormOptions din
     *  @Param CostInterface coin
     *  @Param CfInterface cfin
     *  @Param DurationInterface duin
     *  @Param EffortInterface efin
     * */
    public DormActivity(dormOptions din, CostInterface coin, CfInterface cfin, DurationInterface duin, EffortInterface efin){
	    setup(din,coin,cfin,duin,efin);
    }
    
    /**
     *  setup - assigns private variables and passes proper values to co, cf, duin, and ef
     *  @Param bookstoreName bin
     *  @Param CostInterface coin
     *  @Param CfInterface cfin
     *  @Param DurationInterface duin
     *  @Param EffortInterface efin
     * */
	public void setup(dormOptions din, CostInterface coin, CfInterface cfin, DurationInterface duin, EffortInterface efin){
		d=din;
        co=coin;
        cf=cfin;
        du=duin;
        ef=efin;
        float priceOfdorm = (float)600.50;//Hardcoded value since was not specified in instructions
        if(d == d.GAMING){
        	priceOfdorm += priceOfdorm * (float)1.02;
        	co.addtoCost(priceOfdorm);
            cf.addtoCF((float)10.3);
            du.addtoDuration((float)18.0);
            ef.addtoEffort((float)58.4);	
        }
        else if(d == d.QUEUE){
        	co.addtoCost(priceOfdorm);
            cf.addtoCF((float)50.3);
            du.addtoDuration((float)130.0);
            ef.addtoEffort((float)500.4);	
        }
        else{
        	System.out.println("err");
            System.exit(0);
        }	
	}
	
	
}
