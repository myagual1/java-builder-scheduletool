package studentOrientation.classes;

/**
 *  Interface for cf
 *  Has four functions to be implemented
 * */
public interface CfInterface {
	public float getTotal();
	public void setTotal(float num);
	public void addtoCF(float num);
	public String toString();
}
