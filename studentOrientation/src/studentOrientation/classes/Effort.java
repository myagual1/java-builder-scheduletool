package studentOrientation.classes;

public class Effort implements EffortInterface {
	private float total;

    /**
     *  Constructor - sets inital total to 0
     * */
	public Effort(){
		total = 0;
	}
	
    /**
     *  Constructor - sets intial total to float num
     *  @param float num
     * */
	public Effort(float num){
		total = num;
	}
	
    /**
     *  getTotal
     *  @return total
     * */
	public float getTotal(){
		return total;
	}
	
    /**
     *  setTotal
     *  @param float num
     * */
	public void setTotal(float num){
		total = num;
	}
	
    /**
     *  addToEffort
     *  @param float num
     * */
	public void addtoEffort(float num){
		total += num;
	}
	
    /**
     *  toString
     * */
	public String toString(){
		return "Total effort is " + getTotal() + " calories." ;
	}
	
}
