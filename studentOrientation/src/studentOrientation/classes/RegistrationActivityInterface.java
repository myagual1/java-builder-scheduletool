package studentOrientation.classes;
import studentOrientation.enums.regOptions;

/**
 *  Interface of Registration Activity
 *  Requires one method, setup, to be implemented. It is called in the constructor
 * */
public interface RegistrationActivityInterface{
	public void setup(regOptions r, CostInterface co, CfInterface cf, DurationInterface du, EffortInterface ef);
}
