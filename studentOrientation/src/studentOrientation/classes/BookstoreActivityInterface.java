package studentOrientation.classes;
import studentOrientation.enums.bookstoreName;

/**
 *  BookstoreActivityInterface - interface for Bookstore Activity.
 *  Has one method, setup, which will be called the constructor
 * */
public interface BookstoreActivityInterface{
	public void setup(bookstoreName b, CostInterface co, CfInterface cf, DurationInterface du, EffortInterface ef);
}
