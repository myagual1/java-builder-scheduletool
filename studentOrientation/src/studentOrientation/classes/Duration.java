package studentOrientation.classes;

public class Duration implements DurationInterface {
	private float total;

    /**
     *  Constructer - sets initial total to 0
     * */
	public Duration(){
		total = 0;
	}
	
    /**
     *  Constructer - sets initial total to provided float
     *  @param float num
     * */
	public Duration(float num){
		total = num;
	}
	
    /**
     *  getTotal
     *  @return total
     * */
	public float getTotal(){
		return total;
	}
	
    /**
     *  setTotal
     *  @param float num
     * */
	public void setTotal(float num){
		total = num;
	}
	
    /**
     *  addtoDuration
     *  @param float num
     * */
	public void addtoDuration(float num){
		total += num;
	}
	
    /**
     *  toString
     * */
	public String toString(){
		return "Total duration is " + getTotal() + " minutes.";
	}
	
}	
