package studentOrientation.classes;
import studentOrientation.enums.bookstoreName;

public class BookstoreActivity implements BookstoreActivityInterface{
    private bookstoreName b;
    private CostInterface co; 
    private CfInterface cf; 
    private DurationInterface du; 
    private EffortInterface ef;
    
    /**
     *  Constructor
     *  @Param bookstoreName bin
     *  @Param CostInterface coin
     *  @Param CfInterface cfin
     *  @Param DurationInterface duin
     *  @Param EffortInterface efin
     * */
    public BookstoreActivity(bookstoreName bin, CostInterface coin, CfInterface cfin, DurationInterface duin, EffortInterface efin){
	    setup(bin,coin,cfin,duin,efin);
    }
    
    /**
     *  setup - assigns private variables and passes proper values to co, cf, duin, and ef
     *  @Param bookstoreName bin
     *  @Param CostInterface coin
     *  @Param CfInterface cfin
     *  @Param DurationInterface duin
     *  @Param EffortInterface efin
     * */
	public void setup(bookstoreName bin, CostInterface coin, CfInterface cfin, DurationInterface duin, EffortInterface efin){
         b=bin;
         co=coin;
         cf=cfin;
         du=duin;
         ef=efin;

	     float priceOfTextbooks = (float)150.67;//Hardcoded value since was not specified in instructions
         if(b == bookstoreName.UBS){                                                                                            
             priceOfTextbooks = priceOfTextbooks * (float)1.05;    
         } 
         else if(b == bookstoreName.MBS){
             priceOfTextbooks = priceOfTextbooks;    
         } 
         else{
            System.out.println("err");
            System.exit(0);
         }
         co.addtoCost(priceOfTextbooks);
         cf.addtoCF((float)19.3);
         du.addtoDuration((float)30.0);
         ef.addtoEffort((float)293.4);	
    }
   
}
