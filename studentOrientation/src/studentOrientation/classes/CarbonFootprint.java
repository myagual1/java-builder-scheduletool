package studentOrientation.classes;

/**
 *  Concrete class for the carbon footprint
 * */
public class CarbonFootprint implements CfInterface{
	private float total;
	
    /**
     *  Constructor - initializes CF
     * */
	public CarbonFootprint(){
		total = 0;
	}
	
	/**
     *  CarbonFootprint - sets initial carbon footprint 
     *  @param  float   num
     * */
    public CarbonFootprint(float num){
		total = num;
	}
	
    /**
     *  getTotal - returns total cf
     *  @return total
     * */
	public float getTotal(){
		return total;
	}
	
    /**
     *  setTotal - sets total cf
     *  @param float num
     * */
	public void setTotal(float num){
		total = num;
	}
	
    /**
     *  addtoCF - add to current cf total
     *  @param float num
     * */
	public void addtoCF(float num){
		total += num;
	}
	
	/**
     *  toString
     * */
    public String toString(){
		return "Total carbon footprint is " + getTotal();
	}
}
