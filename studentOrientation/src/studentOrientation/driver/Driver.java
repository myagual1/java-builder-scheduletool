package studentOrientation.driver;
import studentOrientation.enums.*;
import studentOrientation.store.*;
import studentOrientation.util.*;
import studentOrientation.classes.*;

public class Driver{
	public static void main(String args[]) {
        
        //SET ENUMS
        bookstoreName b = bookstoreName.UBS; // UBS or MBS
        tourOptions t = tourOptions.BUS; // BUS or FOOT
        dormOptions d = dormOptions.QUEUE; // QUEUE or GAMING
        regOptions r = regOptions.FORM; // FORM or LAB
        
        //BUILDER PATTERN
        OOItinerary studentItinerary = new Itinerary(b,t,d,r);		
        WorkshopInterface shop = new Workshop();
        shop.construct(studentItinerary);
        
        //PRINT RESULTS
        studentItinerary.returnResults();

	} // end main(...)

} // end public class Driver

